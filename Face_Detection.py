import cv2
import ctypes  # Para caixa de diálogo
import easygui # Para selecionar o arquivo

caminho_arquivo = easygui.fileopenbox()
print(caminho_arquivo [len(caminho_arquivo)-4:len(caminho_arquivo)])

#Verifica a extensão do arquivo - Se for foto, chama o "picture_face_detection"
#Se for video chama o "video_face_detection"
def main(caminho_arquivo):

    if(caminho_arquivo [len(caminho_arquivo)-4:len(caminho_arquivo)] == ".jpg" or caminho_arquivo [len(caminho_arquivo)-4:len(caminho_arquivo)] == ".jpe" or caminho_arquivo [len(caminho_arquivo)-4:len(caminho_arquivo)] == ".tif" or caminho_arquivo [len(caminho_arquivo)-4:len(caminho_arquivo)] == ".png" or caminho_arquivo [len(caminho_arquivo)-4:len(caminho_arquivo)] == ".bmp" or caminho_arquivo [len(caminho_arquivo)-4:len(caminho_arquivo)] == ".dib" or caminho_arquivo [len(caminho_arquivo)-5:len(caminho_arquivo)] == ".tiff" or caminho_arquivo [len(caminho_arquivo)-5:len(caminho_arquivo)] == ".jpeg"):
        img = caminho_arquivo
        picture_face_detection(img)

    elif(caminho_arquivo [len(caminho_arquivo)-4:len(caminho_arquivo)] == ".wmv" or caminho_arquivo [len(caminho_arquivo)-4:len(caminho_arquivo)] == ".avi"):
        img = caminho_arquivo
        video_face_detection(img)
    else:
        ctypes.windll.user32.MessageBoxW(0, "Nenhum arquivo de imagem ou video foi localizado.", "Mensagem", 1)
        return 0

    return img

#Detecção de rostos em imagens
def picture_face_detection(foto):

    #Carregar Imagem02_UsoClassificadorCascade.py
    img = cv2.imread(foto)

    #Cria nosso classificador passando qual o modelo de classificação
    face_classifier = cv2.CascadeClassifier('Haarcascades\haarcascade_frontalface_alt.xml')

    #Converter imagem para escala de cinza
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    #Executar nosso classificador na imagem
    #parâmetros: (imagem, fator de escala - corrige a falsa impressão de um rosto ser maior que o outro
    #por estar mais próximo da câmera, vizinhança - para diferenciar objetos dos rostos)
    #faces = face_classifier.detectMultiScale(gray, 1.4, 2)
    faces = face_classifier.detectMultiScale(gray, 1.1, 3)  # era 1.1

    #Confirma se algum rosto foi encontrado na foto. Caso negativo, muda o classificador
    if (len(faces) == 0):

        ctypes.windll.user32.MessageBoxW(0, "Nenhum rosto foi encontrado. Trocando o classificador...", "Mensagem", 1)

        # Cria novamente o classificador
        face_classifier = cv2.CascadeClassifier('Haarcascades\haarcascade_face.xml')

        # Converte novamente a imagem para escala de cinza
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        #Executa nosso classificador na imagem
        #parâmetros: (imagem, fator de escala - corrige a falsa impressão de um rosto ser maior que o outro
        #por estar mais próximo da câmera, vizinhança - para diferenciar objetos dos rostos)
        #faces = face_classifier.detectMultiScale(gray, 1.4, 2)
        faces = face_classifier.detectMultiScale(gray, 1.1, 3)  # era 1.1

        #Extração dos boxes encontrados//;;
        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.imshow('Rosto', img)

        print("final")

        cv2.waitKey(0)
        cv2.destroyAllWindows()

        return 0

    #Extração dos boxes encontrados//;;
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
        cv2.imshow('Rosto', img)

    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return 0

#Detecta rostos em videos
def video_face_detection(video):
    video = cv2.VideoCapture(video)

    # Confirma se o video foi carregado
    if (video.isOpened() == False):
        print("Error opening video stream or file")

    # Criar nosso classificador, passando qual o modelo de classificação
    face_classifier = cv2.CascadeClassifier('Haarcascades\haarcascade_face.xml')

    # Lê até que o video tenha terminado
    while (video.isOpened()):
        # Lê frame por frame
        ret, frame = video.read()
        if ret == True:

            #Converter frames para escala de cinza
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            #Aplica o classificador em uma variável
            faces = face_classifier.detectMultiScale(gray, 1.1, 3)  # era 1.1

            #Extração dos boxes encontrados
            for (x, y, w, h) in faces:
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 255), 2)
            cv2.imshow('Video', frame)

            #Se apertar o Q no teclado, o video acaba
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break

        #Encerra o loop
        else:
            break

    #No final, libera o video da memória
    video.release()

    cv2.waitKey(0)
    cv2.destroyAllWindows()

#Chamada da função principal passando como atributo o caminho da imagem
main(caminho_arquivo)