import cv2

#Camêra primária
cap = cv2.VideoCapture(0)

#Se a câmera estiver aberta...
#Criar nosso classificador, passando qual o modelo de classificação
face_classifier = cv2.CascadeClassifier('Haarcascades\haarcascade_face.xml')

#Lê até que o video tenha terminado
while (cap.isOpened()):
    #Lê frame por frame
    ret, frame = cap.read()
    if ret == True:

        #Converter frames para escala de cinza
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        #Aplica o classificador em uma variável
        faces = face_classifier.detectMultiScale(gray, 1.1, 3)  # era 1.1

        #Extração dos boxes encontrados
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 255), 2)
        cv2.imshow('Video', frame)

        #Se apertar o Q no teclado, o video acaba
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break

    #Encerra o loop
    else:
        break

#No final, libera o video da memória
cap.release()
cv2.destroyAllWindows()