import sys
from easygui import * #Traz toda a importação do EasyGUI

while 1:
    msgbox("Bem vindo ao projeto da APS 2º Semestre 2017") #Exibe uma caixa de mensagem.

    #Abre uma caixa de seleção para respostas.
    msg ="De que forma você gostaria de identificar um rosto?"
    title = "Seleção"
    choices = ["Por Web Cam", "Selecionar FOTO/VIDEO"]
    choice = choicebox(msg, title, choices)


    if str(choice) == "Por Web Cam":
        msg = "Você escolheu utilizar a WEBCAM, quer continuar?"
        title = "Confirmação"
        if ccbox(msg, title):     #mostre uma caixa de diálogo Continuar / Cancelar
            import Teste_camera   #usuário escolhe Continuar
        else:
            sys.exit(0)           #o usuário escolheu Cancelar

    else:
        msg = "Você escolheu selecionar um ARQUIVO/VIDEO, quer continuar?"
        title = "Confirmação"
        if ccbox(msg, title):      #mostre uma caixa de diálogo Continuar / Cancelar
            import Face_Detection  #usuário escolhe Continuar
        else:
            sys.exit(0)            #o usuário escolheu Cancelar